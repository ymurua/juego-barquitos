/**
 * 
 */
package movimiento;

import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 * Representa a todos los objetos que se mueven con cada tick del reloj
 *
 */
public abstract class ElementoMovil {

	protected Posicion posicion;
	private final int velocidad= 1; //TODO mover todos los numeros a clase de Constantes / Configuracion
	
	
	/**
	 * @param posicion
	 * @param velocidad
	 */
	protected ElementoMovil(Posicion posicion) {
		this.posicion = posicion;
	}

	public Posicion getPosicion() {
		return posicion;
	}

	protected int getVelocidad() {
		return velocidad;
	}
	 
	protected void avanzar(){
		this.posicion.actualizar(posicion.getX()+velocidad,posicion.getY());
	}
	protected void avanzarV(){
		this.posicion.actualizar(posicion.getX(),posicion.getY()+velocidad);
	}

	public void paint(Graphics grafica) {
		// TODO Auto-generated method stub
		
	}

	public void paint(Graphics2D g) {
		// TODO Auto-generated method stub
		
	}

}
