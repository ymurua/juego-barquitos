package clases;


import java.awt.Image;
//import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;

import movimiento.ElementoMovil;
import movimiento.Posicion;

public class Jugador extends ElementoMovil{
	
	private int  ancho, altura, cox, coy; //posicion
	public double velX, velY; //VELOCIDAD
	private Image imag ,newimg;
	//ControladordeSesion parent; 
	int damage; 
	private boolean dead; 
	
	public Jugador(){
		super(new Posicion(300,500));
		

		this.ancho = 75; //ancho de la imagen
		this.altura = 75;// altura de la imagen
		cox = 30;
		coy = 45;
		this.damage = 0; // ver si hay que borrar
		this.dead=false;
		
		//Imagen del ca�on.
		ImageIcon imageIcon= new ImageIcon("C:\\Users\\Nayla Cheheid\\Desktop\\naylacheheid\\src\\clases\\canon.png");
		this.imag=imageIcon.getImage();
		this.newimg = imag.getScaledInstance(80, 80,  java.awt.Image.SCALE_SMOOTH);
			
	}


	
	
	//Getters
	public int getX(){
		return getPosicion().getX();
	}
	
	public int getY(){
		return getPosicion().getY();	
	}
	
	public int getcX(){
		return cox;
	}
	
	public int getcY(){
		return coy;
	}
	
	public int getAltura(){
		return altura;
	}
	
	public int getAncho(){
		return ancho;
	}
	
	public Image getImag(){
		return newimg;
	}
	
	public void run(){
		this.getPosicion().actualizar(this.getPosicion().getX(), 
				this.getPosicion().getY());
	}

	
	public boolean isDead(){
		return this.dead;
	}
	
	public void impactaJugador(){
		this.dead=true;
	}
	
	
	/*		//MOVIMIENTOOOOOO
	// M�todos que se implementan en JugadorMovil para que responda a teclado.
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}





	public void run(int direccion) {
		// TODO Auto-generated method stub
		
	}
	*/
	
}
