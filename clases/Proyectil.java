package clases;

import java.awt.Image;

import javax.swing.ImageIcon;

//import naveEnemiga.boolan;
import movimiento.ElementoMovil;
import movimiento.Posicion;

public class Proyectil extends ElementoMovil{
	
	//public int x, y; //position
	double vx, vy;
	private int poyectilPropio;  // 1 propio , 2 enemigo
	
	private Image imag ,newimg;
//	private int dx;
	
	
	public Proyectil(int x, int y, int vx, int vy, int propio){
		super(new Posicion(x, y));
		
//		this.x = x; //inicio
//		this.y = y;
		this.vx = vx;//fin en 0 el tirador tira solo recto
		this.vy = vy;
		this.poyectilPropio=propio; //tipo
	
		
		ImageIcon imageIcon= new ImageIcon("C:\\Users\\Nayla Cheheid\\Desktop\\naylacheheid\\src\\clases\\misil.png");
		this.imag=imageIcon.getImage();
		this.newimg = imag.getScaledInstance(20, 20,  java.awt.Image.SCALE_SMOOTH);
	}
	
	
	public void run(){ // avanza el proyectil.
		this.posicion.actualizar(this.posicion.getX()+(int)vx, this.posicion.getY()+(int)vy);
	
	}
	

	public int valor(){
		
		return poyectilPropio;
	
	}
	
	public Image getImag(){
		return newimg;
	}
	
}
