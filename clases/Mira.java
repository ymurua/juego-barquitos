package clases;

import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.ImageIcon;

import movimiento.ElementoMovil;
import movimiento.Posicion;


public class Mira extends ElementoMovil implements MouseMotionListener{

	private Image imag ,newimg;
	private int dx;
				
	
	
	public Mira (){
		super(new Posicion(0, 50));
		
		ImageIcon imageIcon= new ImageIcon("C:\\Users\\Nayla Cheheid\\Desktop\\naylacheheid\\src\\clases\\mira.jpg");
		this.imag=imageIcon.getImage();
		this.newimg = imag.getScaledInstance(50, 50,  java.awt.Image.SCALE_SMOOTH);
	}
	
	public Posicion getPosicion() {
		return posicion;
	}
		
	public int getX(){
		return posicion.getX();
	}
	
	public int getY(){
		return posicion.getY();
	}
	
	public void run(int x,int y) {		
		this.posicion.actualizar(x,100);

		//Para que no se vaya de la pantalla en X
		if(this.posicion.getX() > 650){
			this.posicion.actualizar(650, this.posicion.getY());

		}else if( this.posicion.getX() < 0){
			this.posicion.actualizar(0, this.posicion.getY());		
		}

	}
	
	public Image getImag(){
		return newimg;
	}

	
	
	//Movimientos de mouse
	@Override
	public void mouseMoved(MouseEvent event) {
	    
		dx=event.getX();

	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
}
