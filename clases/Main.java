package clases;

import test.PantallaJuego;

public class Main{
    public static void main(String[] args){
		
		javax.swing.JFrame f = new javax.swing.JFrame("Ca�on vs Barco");
		
		f.setVisible(true);
		f.setDefaultCloseOperation(f.EXIT_ON_CLOSE);
		f.setResizable(false);
		f.setLocationRelativeTo(null);
		
		f.getContentPane().setLayout(new java.awt.BorderLayout());
		
		new PantallaJuego();
    }
}