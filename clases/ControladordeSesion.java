package clases;


import java.awt.Image;

import Enemigo.accionBarco;
import Enemigo.barco;
import movimiento.Posicion;

public class ControladordeSesion {
	
	
	private Jugador jugador;  // PONER PRIVADO
	
	java.util.Vector<accionBarco> enemigos;
//	private NaveNoDispara enemigo;
	private barco enemigo;
	private java.util.Vector<Proyectil> proyectiles; // hacer un metodo que lo devuelva
	private Mira mira;
	private Tiempo tiempo;
	
	private Proyectil proyectil;
	///javax.swing.JFrame f; 
	//public java.awt.Point mira;		 // E MIRA
	int health; 
	int frame; 
	private int level=1;
	//int xp; 
	boolean levelup=false; 
	private int puntos;
	//int cooldown; 
	int tiempoEspera; 

	int destruidas;
	private int vidas;	
	int barquitos;
	int dificultad;
	int xdiff;
	int ydiff;
	boolean dead;
	int naveinicial=2;
	public ControladordeSesion(int dificultad ){
			
		
		proyectiles = new java.util.Vector<Proyectil>(); //Creo el vector de proyectiles
		tiempo= new Tiempo();
		//tiempo.Contar();		//enemigos = new java.util.Vector<NaveNoDispara>(); //Creo el vector de proyectiles
		mira = new Mira();
	
	//	enemigo=new NaveNoDispara();
		
		if (dificultad==1){
			enemigo= new accionBarco();			//QUE VAYA MAS RAPIDO
			jugador = new Jugador();
			}else{
				jugador = new JugadorMovil();
				if(dificultad==2){
				enemigo= new accionBarco();		//QUE VAYA MAS RAPIDO!!!!!!!!!!!!!!1
				}
				if(dificultad==3){
				enemigo= new accionBarco();		//QUE VAYA MAS RAPIDO
				}
			}
		
				
		frame = 0;

		puntos = 0;
		
		tiempoEspera = 0;
		barquitos=2;
		destruidas=0;
		vidas=3; // La cantidad de vidas inicializa en 1' Calcular las vidas +1 cuando suma  puntos
		// -1 cuando no tira 10 barcos o cuando le pega un proyectil
		
	}

	
	public int getPuntos(){
		return puntos;
	}
	
	public int getVidas(){
		return vidas;
	}
	
	public int getNivel(){
		return level;
	}
	
	
	//JUGADOR//
	public int getJugadorX(){
		return jugador.getX();
	}
	
	public int getJugadorY(){
		return jugador.getY();
	}
	
	public Image getJugadorImage(){
		return jugador.getImag();
	}

	public int getJugadorcX(){
		return jugador.getcX();
	}
	
	public int getJugadorcY(){
		return jugador.getcY();
	}
	
	
	public int getProyectilX(int i){ //Tengo que pasarle la posicion del vector donde esta el enemigo
		return proyectiles.get(i).getPosicion().getX();
	}
	
	public int getProyectilY(int i){ //Tengo que pasarle la posicion del vector donde esta el enemigo
		return proyectiles.get(i).getPosicion().getY();
	}
	
	public Image getProyecImage(){
		return proyectil.getImag();
	}
	
	
	public Jugador obtenerJugador(){
		return jugador;
	}
	
	//public void moverJugador(int direccion){
	//	jugador.run(direccion);
	//}
	
	//MIRA//
	
	public Mira obtenerMira(){
		return mira;
	}
	
	public void moverMira(int x,int y){
		mira.run(x,y);
	}
	
	public int getMiraX(){
		return mira.getX();
	}
	
	public int getMiraY(){
		return mira.getY();
	}
	
	public int getSegundos(){
		return tiempo.getSegundos();
		
	}
	
	public void contar(){
		tiempo.Contar();
	}
	public void detenerContador(){
		tiempo.Detener();
	}
	
		//Barco ENEMIGO//
	public barco Enemigo(){
		return enemigo;
		
	}
	
	public Image getEnemigoImagen(){
	
		return enemigo.getImag();
		
	}
	
	
	
	public int getEnemigoX(){
		return enemigo.getPosicion().getX();
	}
	public int getEnemigoY(){
		return enemigo.getPosicion().getY();
	}
	
	public void runEnemigo(){
		enemigo.run();
		if(enemigo.getPosicion().getX() > 700 && barquitos>1){ //pasan 20 naves.
			enemigo.reiniciar();
			barquitos --;
	}

	}
	
	public void ReiniciarEnemigo(){//cuando pasa de nivel.
		barquitos= 5;
	}
	
	public int getNaves(){
		return barquitos;
	}
	
	//MISILES
	public java.util.Vector<Proyectil> getProyectiles(){
		return proyectiles;
	}
	
	public int cantidadProyectiles(){
		return proyectiles.size();
		
	}
	
	public void avanzarProyectil(int i){
		proyectiles.get(i).run();
		
	}
	
	
	public void runProyectiles(){
		
		if( proyectiles.size()>0){
			
			for(int i = 0; i < proyectiles.size(); i++){
			proyectiles.get(i).run();
			
			if(proyectiles.get(i).getPosicion().getX() > 700 || proyectiles.get(i).getPosicion().getY() > 600 || proyectiles.get(i).getPosicion().getX() < 0 || proyectiles.get(i).getPosicion().getY() < 0){
					proyectiles.remove(i);
					i--;
				}
			}
		}
	}
	
	
	public void Disparar(){
			int xdiff =mira.getX()-(jugador.getX()+jugador.getcX());
			int ydiff =mira.getY()-(jugador.getY()+jugador.getcY());	
			double distancia = Math.sqrt(xdiff*xdiff+ydiff*ydiff);
			
			proyectiles.add(new Proyectil(jugador.getX(), jugador.getY(), (int)(xdiff*(5/distancia)), (int)(ydiff*(5/distancia)), 1));			
		
	}
		
	public void impacto(){
		
	for(int i = 0; i < proyectiles.size(); i++){ //mientras haya poyectiles en juego
//			
		if(proyectiles.get(i).valor()==2){ //no es propia es enmigo
				
				 xdiff = (jugador.getX()+jugador.getcX())-(proyectiles.get(i).getPosicion().getX());
				 ydiff = (jugador.getY()+jugador.getcY())-(proyectiles.get(i).getPosicion().getY());
				
				if(Math.abs(xdiff) < 50 && Math.abs(ydiff) < 50){
					
					proyectiles.remove(i);
					i--;						
					vidas= vidas-1;
					
				}
				calcularNivel();
		}
				else{ //el proyectil es propio!
					
					xdiff = (enemigo.getPosicion().getX()+enemigo.getCox())-(proyectiles.get(i).getPosicion().getX());
					ydiff = (enemigo.getPosicion().getY()+enemigo.getCoy())-(proyectiles.get(i).getPosicion().getY());
					
					if(Math.abs(xdiff) < 50 && Math.abs(ydiff) < 50){							
							proyectiles.remove(i);					
							i--;
							puntos += 20;//*level;
							destruidas = destruidas + 1;
							enemigo.reiniciar();
							dead=true;
							

						
							barquitos= barquitos-1;
							
							calcularNivel();
							
							
//							
//							if(puntos>90*level){
//								level++;
//								levelup = 150;
//							}
							}	
					
					}
			
				}
			}
	
		public void calcularNivel(){
			if( barquitos==0 && destruidas>=naveinicial){
				level= level+1;
				ReiniciarEnemigo();
				levelup=true;
			}
//			if(vidas==0){
//				enemigo=null;
//				jugador=null;
//			}
		
		}
		
		public boolean levelUp(){
			
			return levelup;
		}

		public Image getProyectilImagen(int j) {
			
			return this.getProyectiles().get(j).getImag();
		}

		public Image getMiraImagen() {
			
			return obtenerMira().getImag();
		}

		public Image getJugadorImagen() {
			
			return obtenerJugador().getImag();
		}
		

} 
	


	



