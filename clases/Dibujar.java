package clases;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.MouseInfo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.DefaultFocusManager;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

import clases.Jugador;


public class Dibujar extends JPanel implements ActionListener {
		
	private static final long serialVersionUID = 1L;
		
	private Timer timer;
		
	private ControladordeSesion padre;
		
	//private int direccion; // 1  derecha , 2  izquierda
	
	private JLabel texto;
	private JLabel subirNivel;
		 
	private boolean disparo= false;

	private String nivelDificultad;
	Color celeste = new Color(137,206,230);
		
		public Dibujar(int dificultad){
			
			this.padre=new ControladordeSesion(dificultad); //le paso al controlador la dificultad
			this.setFocusable(true);	

			if(dificultad==1){
				setBackground(celeste);
				nivelDificultad="Nivel 1";
				timer= new Timer (34,this);
				timer.start();
			}
				
			if(dificultad==2){
				setBackground(celeste);
				nivelDificultad="Nivel 2";
				timer= new Timer (12,this);
				timer.start();
				}
				
			if(dificultad==3){
				setBackground(celeste);
				nivelDificultad="Nivel 3";
				timer= new Timer (2,this);
				timer.start();
				}
				
			
			texto = new JLabel();
			subirNivel= new JLabel();
			
			setFocusable(true);	
			
			
			setFocusable(true);
			addKeyListener(new teclado());
			
		//	timer= new Timer (15,this);
		//	timer.start();
			
			padre.contar();
			
		
		}
		
		
		public void paint(Graphics grafica){// grafica mira
			
			super.paint(grafica);
			
			Graphics2D enemigo= (Graphics2D) grafica;
		
			enemigo.drawImage(padre.getEnemigoImagen(), padre.getEnemigoX(), padre.getEnemigoY(), null);
	
			Graphics2D jug= (Graphics2D) grafica;
			jug.drawImage(padre.getJugadorImagen(), padre.getJugadorX(), padre.getJugadorY(), null);

			Graphics2D jugMira= (Graphics2D) grafica;
			jugMira.drawImage(padre.getMiraImagen(), padre.getMiraX(), padre.getMiraY(), null);

			Graphics2D jugBala= (Graphics2D) grafica;
			
			if( padre.cantidadProyectiles()>0){

			for(int j = 0; j <padre.cantidadProyectiles(); j++){

				jugBala.drawImage(padre.getProyectilImagen(j),padre.getProyectilX(j),
						padre.getProyectilY(j), null);		
			}
			
		}
			
		}
		
		public void actionPerformed(ActionEvent e){
			
			int x = MouseInfo.getPointerInfo().getLocation().x-350;
	    	int y = MouseInfo.getPointerInfo().getLocation().y;
	    	
			padre.runEnemigo();
			padre.moverMira(x,y);
			padre.runProyectiles();
			padre.impacto();
			this.texto.setText(nivelDificultad+"     Puntos: "+String.valueOf(padre.getPuntos())+
					"       Niveles jugados: "+String.valueOf(padre.getNivel())+"      Vidas:  "+String.valueOf(padre.getVidas()));
			add(texto);
			
			
			if (disparo && padre.getSegundos()>1){
				disparo=false;
				padre.Disparar();
				padre.detenerContador();
				padre.contar();
				
			} else {
				disparo=false;
			}
			
//			
//			if(padre.levelup){
//				padre.detenerContador();
//				padre.contar();
//	
//				this.subirNivel.setText("Subiste de nivel!");
//				add(subirNivel);
//				
//				if(padre.getSegundos()>5){
//					remove(subirNivel);
//				}
//			}
			
			repaint();
	
			
		}
		
		
		
		/// Esto hace que el jugador se mueva y que dispare :
		private class teclado extends KeyAdapter{ //tocar y soltar
			
			public void keyReleased(KeyEvent e){
				int key=e.getKeyCode();
		/*		if(key==KeyEvent.VK_LEFT)
					padre.moverJugador(2); 
				if(key==KeyEvent.VK_RIGHT)
					padre.moverJugador(1); */
				if(key==KeyEvent.VK_SPACE)
					disparo=true;
				revalidate();
                repaint();
			}	
		
			//MOVIMIENTOOOOOO		
	//		public void keyPressed(KeyEvent e){  //cuando se mantiene apretado el teclado
	//			int key=e.getKeyCode();
				
	//			if(key==KeyEvent.VK_LEFT)
	//				padre.moverJugador(2); 
	//			if(key==KeyEvent.VK_RIGHT)
	//				padre.moverJugador(1); 
	//		}
			
			
		}

		
}
		





