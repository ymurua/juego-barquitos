package Enemigo;


import java.awt.Image;

import javax.swing.ImageIcon;

import movimiento.ElementoMovil;
import movimiento.Posicion;

public abstract class barco extends ElementoMovil {
	
	private Image imag ,newimg;
	protected int cox=20;
	protected int coy=20;
	
	protected barco() {
		super(new Posicion(-30,50));
		
		ImageIcon imageIcon= new ImageIcon("C:\\Users\\Nayla Cheheid\\Desktop\\naylacheheid\\src\\Enemigo\\barco.jpg");
		this.imag=imageIcon.getImage();
		this.newimg = imag.getScaledInstance(80, 80,  java.awt.Image.SCALE_SMOOTH);
		
	}
	
	public void run() {
		this.getPosicion().actualizar(this.getPosicion().getX() + this.getVelocidad(), 
		this.getPosicion().getY());
	}
	
	public Image getImag(){
		return newimg;
	}
	
	public Posicion getPosicion() {
		return posicion;
	}
		
	
	public int getCox(){
		return cox;
	}
	
	public int getCoy(){
		return coy;
	}
	
	public void reiniciar(){
		this.getPosicion().actualizar(-30, 
				this.getPosicion().getY());
	}


}

