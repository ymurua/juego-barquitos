package test;

import javax.swing.*;

import clases.ControladordeSesion;
import clases.Dibujar;

import java.awt.*;
import java.awt.event.*;
public class elegir_nivel extends JFrame implements ActionListener{
    private JMenuBar mb;
    private JMenu menu1;
    private JMenuItem mi1,mi2,mi3;
    private Dibujar dibujar;
    
    public elegir_nivel() {
        setLayout(null);
        mb=new JMenuBar();
        setJMenuBar(mb);
        menu1=new JMenu("Opciones");
        mb.add(menu1);
      
        mi1=new JMenuItem("N1");
        mi1.addActionListener(this);
        menu1.add(mi1);
        mi2=new JMenuItem("N2");
        mi2.addActionListener(this);
        menu1.add(mi2);
        mi3=new JMenuItem("N3");
        mi3.addActionListener(this);
        menu1.add(mi3);               
        
    }
    
    
    //Parrafo innecesario porque decidi no cambiar los colores
    public void actionPerformed(ActionEvent e) {
    	Container f=this.getContentPane();
        if (e.getSource()==mi1) {
        	dibujar=new Dibujar(2);
			setContentPane(dibujar);
			
			dibujar.setFocusable(true);
            dibujar.requestFocusInWindow();
			invalidate();
			validate();
  	      new PantallaJuego();
        }
        if (e.getSource()==mi2) {
            f.setBackground(new Color(0,255,0));
        }
        if (e.getSource()==mi3) {
            f.setBackground(new Color(0,0,255));
        }        
    }
    
    
    
    public static void main(String[] ar) {
    	elegir_nivel opcion1=new elegir_nivel();
        opcion1.setBounds(10,20,300,200);
        opcion1.setVisible(true);
    }    
}