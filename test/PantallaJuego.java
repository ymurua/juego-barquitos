
package test;

import java.awt.Color;
import java.awt.Container;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import clases.ControladordeSesion;
import clases.Dibujar;


public class PantallaJuego extends JFrame{
	
	 MenuBar barra = new MenuBar();
	 Menu instrucciones = new Menu("Instrucciones");
	 Menu ranking = new Menu("Ranking");
	 Menu salir = new Menu("Salir");
	 Menu N1 = new Menu("Nivel 1");
	 Menu N2 = new Menu("Nivel 2");
	 Menu N3 = new Menu("Nivel 3");
	
	 Color celeste = new Color(137,206,230);
	
	 ControladordeSesion controlador;
	 Dibujar dibujar;
	 
	 private JButton btnN1;
	 private JButton btnN2;
	 private JButton btnN3;
	 private JButton btnEmpezar;
	 
	 private int dificultad;

	 private JLabel texto ;
	
	 JLabel dif1= new JLabel();
	 JLabel dif2= new JLabel();
	 JLabel dif3= new JLabel();
	 
	
	 public PantallaJuego( ){
		 super("Ca�on vs Barco");
		
		
	     //SELECCIONAMOS LA BARRA DE MENUS
	      setMenuBar(barra);

	      //A LA BARRA LE AGREGAMOS LOS MENUS.
	      barra.add(instrucciones);
	      barra.add(ranking);
	      barra.add(salir);
	 
	 
	      //PARA AGREGAR UN SEPARADOR ENTRE VARIAS OPCIONES DEL MENU, 
	      //HACEMOS LO SIGUIENTE

	 
	      //AHORA VAMOS A AGREGARLE OPCIONES AL MENU DE AYUDA
	      instrucciones.add("Explicacion juego");
	      
	      //AHORA VAMOS A AGREGARLE OPCIONES AL RANKING
	      ranking.add("Ver top 10");
	      

	      
	      salir.add("Salir");
	    
	      Container c = this.getContentPane();
			c.setLayout(null);
			c.setBackground(celeste);
			
	      salir.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
			System.exit(0);
   
			}
	      });
	    
	      N1.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent arg0) {
					PantallaJuego.this.dibujar=new Dibujar(1);
					PantallaJuego.this.setContentPane(dibujar);
					
					dibujar.setFocusable(true);
		            dibujar.requestFocusInWindow();
					PantallaJuego.this.invalidate();
					PantallaJuego.this.validate();
	   
				}
		      });
	      N2.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent arg0) {
					PantallaJuego.this.dibujar=new Dibujar(2);
					PantallaJuego.this.setContentPane(dibujar);
					
					dibujar.setFocusable(true);
		            dibujar.requestFocusInWindow();
					PantallaJuego.this.invalidate();
					PantallaJuego.this.validate();
	   
				}
		      });
	      N3.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent arg0) {
					PantallaJuego.this.dibujar=new Dibujar(3);
					PantallaJuego.this.setContentPane(dibujar);
					
					dibujar.setFocusable(true);
		            dibujar.requestFocusInWindow();
					PantallaJuego.this.invalidate();
					PantallaJuego.this.validate();
	   
				}
		      });
	 
	    texto = new JLabel();
	  	btnN1 = new JButton("Nivel 1");
		btnN2 = new JButton("Nivel 2");
		btnN3 = new JButton("Nivel 3");
		btnEmpezar = new JButton("JUGAR");
		

		texto.setBounds(260,130,400,100);		
		btnN1.setBounds(120,250,150,70);
		btnN2.setBounds(270,250,150,70);
		btnN3.setBounds(420,250,150,70);
		btnEmpezar.setBounds(250,400,200,50);
	  
		this.texto.setText("Elija el nivel en que desea jugar:");
		texto.setForeground(Color.BLACK);
		
		btnN1.setFocusable(false);
		
		btnN1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dificultad=1;
				
				
				dif1.setText("Modalidad nivel 1");
				dif1.setBounds(300,350,400,50);
				dif1.setForeground(Color.BLACK);
				PantallaJuego.this.add(dif1);
				PantallaJuego.this.validate();
				PantallaJuego.this.repaint();
				PantallaJuego.this.remove(dif2);
				PantallaJuego.this.remove(dif3);
			}
		});
		
	btnN2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dificultad=2;
				
				
				dif2.setText("Modalidad nivel 2");
				dif2.setBounds(300,350,400,50);
				dif2.setForeground(Color.BLACK);
				PantallaJuego.this.add(dif2);
				PantallaJuego.this.validate();
				PantallaJuego.this.repaint();
				
				PantallaJuego.this.remove(dif1);
				PantallaJuego.this.remove(dif3);
				
			}
		});
		
	btnN3.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			dificultad=3;
			
			
			dif3.setText("Modalidad nivel 3");
			dif3.setBounds(300,350,400,50);
			dif3.setForeground(Color.BLACK);
			PantallaJuego.this.add(dif3);
			PantallaJuego.this.validate();
			PantallaJuego.this.repaint();
			

			PantallaJuego.this.remove(dif1);
			PantallaJuego.this.remove(dif2);
			
		}
	});
	
	btnEmpezar.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			PantallaJuego.this.dibujar=new Dibujar(dificultad);
			PantallaJuego.this.setContentPane(dibujar);
			
			dibujar.setFocusable(true);
            dibujar.requestFocusInWindow();
			PantallaJuego.this.invalidate();
			PantallaJuego.this.validate();
		
			
		}
	});

		c.add(texto);
		c.add(btnN1);
		c.add(btnN2);
		c.add(btnN3);
		c.add(btnEmpezar);
		
			
	 }


	public static void main (String[] args){
		   
		  PantallaJuego p= new PantallaJuego();
			 p.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			 p.setResizable(false);
		     p.setSize(700, 700);
			 p.setLocationRelativeTo(null);
		     p.setVisible(true);

		      
		    
		     
		     
		
	   }
}
